package Regina;

import java.io.Serializable;

public class Book implements Serializable{
    private static final long serializableUID = 1L;
    private int isbn;
    private String name;
    private int year;

    public Book(int isbn, String name, int year) {
        this.isbn = isbn;
        this.name = name;
        this.year = year;
    }

    public int getIsbn() {
        return isbn;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Book{" +
                "isbn=" + isbn +
                ", name='" + name + '\'' +
                ", year=" + year +
                '}';
    }
}
