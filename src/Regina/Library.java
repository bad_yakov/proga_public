package Regina;


import java.util.List;
import java.util.Map;

public interface Library {

    void add(String author, Book book);

    Map<String, List<Book>> getAll();

    Book getBook(String author, int isbn);

    void remove(String author, int isbn);

    List<Book> get(String author);

}
