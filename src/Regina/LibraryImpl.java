package Regina;


import java.io.*;
import java.util.*;

public class LibraryImpl implements Library {
    private InputStream inputStream = null;
    private OutputStream outputStream = null;
    private ObjectInputStream ois = null;
    private ObjectOutputStream oos = null;
    private Map<String, List<Book>> map;

    public LibraryImpl() {

    }

    @Override
    public void add(String author, Book book) {
        ArrayList<Book> local;
        map = null;
        try {
            map = readMap();
            if (map != null) {
                if (map.containsKey(author)) {
                    local = (ArrayList) map.get(author);
                    local.add(book);
                    local.sort(new Comparator<Book>() {
                        @Override
                        public int compare(Book o1, Book o2) {
                            if (o1.getYear() == o2.getYear())
                                return 0;
                            if (o2.getYear() < o1.getYear())
                                return 1;
                            else
                                return -1;
                        }
                    });
                    map.put(author, local);
                } else {
                    local = new ArrayList<Book>();
                    local.add(book);
                    map.put(author, local);
                }
            } else {
                local = new ArrayList<Book>();
                local.add(book);
                map = new TreeMap<String, List<Book>>();
                map.put(author, local);
            }
            writeMap(map);
        } catch (Exception e) {

        }
    }

    @Override
    public Map<String, List<Book>> getAll() {
        map = readMap();
        return map;
    }

    @Override
    public Book getBook(String author, int isbn) {
        try {
            map = readMap();
            ArrayList<Book> local = (ArrayList<Book>) map.get(author);
            for (int i = 0; i < local.size(); i++) {
                if (local.get(i).getIsbn() == isbn) {
                    return local.get(i);
                }
            }
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public List<Book> get(String author) {
        try {
            map = readMap();
            List<Book> local = map.get(author);
            return local;
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public void remove(String author, int isbn) {
        try {
            map = readMap();
            ArrayList<Book> local = (ArrayList<Book>) map.get(author);
            int removeId = 0;
            for (int i = 0; i < local.size(); i++) {
                if (local.get(i).getIsbn() == isbn) {
                    removeId = i;
                    break;
                }
            }
            local.remove(removeId);
            map.put(author, local);
            writeMap(map);
        } catch (Exception e) {

        }
    }


    private Map<String, List<Book>> readMap() {
        Map<String, List<Book>> result = null;
        try {
            inputStream = new FileInputStream("library.txt");
            ois = new ObjectInputStream(inputStream);
            result = (Map<String, List<Book>>) ois.readObject();
        } catch (Exception e) {

        }
        return result;
    }

    private void writeMap(Map<String, List<Book>> localMap) {
        try {
            outputStream = new FileOutputStream("library.txt");
            oos = new ObjectOutputStream(outputStream);
            oos.writeObject(localMap);
        } catch (Exception e) {

        }
    }
}
