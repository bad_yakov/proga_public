package phoneBook;


import phoneBook.Entities.Person;
import phoneBook.Repositories.PhoneBookImpl;


public class Main {

    public static void main(String[] args) throws Exception {
        PhoneBookImpl pb = new PhoneBookImpl();
        Person person = new Person(1, "Бадыгин", "Яков", "Васильевич", 18, 89600739937L);

        pb.add(person.getLastName(), person);
        System.out.println(pb.getAll().get(0));
    }
}
