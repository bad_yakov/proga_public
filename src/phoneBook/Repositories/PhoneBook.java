package phoneBook.Repositories;


import phoneBook.Entities.Person;

import java.util.List;


public interface PhoneBook {
    boolean add(String key,Person person);

    Person getPersonsByKey(String key);

    List<Person> getAll();
}
