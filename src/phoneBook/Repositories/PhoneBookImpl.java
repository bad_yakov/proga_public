package phoneBook.Repositories;

import phoneBook.Entities.Person;

import java.io.*;
import java.util.*;

public class PhoneBookImpl implements PhoneBook {
    private InputStream inputStream = null;
    private OutputStream outputStream = null;
    private ObjectInputStream ois = null;
    private ObjectOutputStream oos = null;
    private Map<String, Person> map;

    public PhoneBookImpl() {
    }

    @Override
    public boolean add(String key, Person person) {
        map = null;
        try {
            map = readMap();
            if (map != null) {
                map.put(key, person);
            } else {
                map = new TreeMap<String, Person>();
                map.put(key, person);
            }
            writeMap(map);
            return true;
        } catch (Exception e) {

        }
        return false;
    }

    @Override
    public Person getPersonsByKey(String key) {
        map = readMap();
        if (map != null)
            return map.get(key);
        return null;
    }

    @Override
    public List<Person> getAll() {
        map = readMap();
        if (map != null) {
            ArrayList<Person> local = new ArrayList<Person>();
            for(Map.Entry entry : map.entrySet()){
                local.add((Person) entry.getValue());
            }
            return local;
        }
        return null;
    }

    private Map<String, Person> readMap() {
        Map<String, Person> result = null;
        try {
            inputStream = new FileInputStream("phone.txt");
            ois = new ObjectInputStream(inputStream);
            result = (Map<String, Person>) ois.readObject();
        } catch (Exception e) {

        }
        return result;
    }

    private void writeMap(Map<String, Person> localMap) {
        try {
            outputStream = new FileOutputStream("phone.txt");
            oos = new ObjectOutputStream(outputStream);
            oos.writeObject(localMap);
        } catch (Exception e) {

        }
    }
}
